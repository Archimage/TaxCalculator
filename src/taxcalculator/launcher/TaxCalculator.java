/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxcalculator.launcher;

import java.util.Scanner;
import taxcalculator.internal.TaxAmount;

/**
 *
 * @author Archimage
 */
public class TaxCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean retry = true;
        while(retry){
            System.out.println("Possible values of filing status:" 
                         + System.lineSeparator()
                         + "0 - Single filer" + System.lineSeparator()
                         + "1 - Married jointly or qualifying widow(er)" 
                         + System.lineSeparator()
                         + "2 - Married separately" + System.lineSeparator()
                         + "3 - Head of household");
            //Ask for user status
            System.out.print("Enter your filing status: ");
            int status;
            //Create a variable to store the amount of tax
            Scanner input = new Scanner(System.in);
            status = input.nextInt();
            //Ask for user income
            switch(status){
            case 0:
            case 1:
            case 2:
            case 3:
                System.out.print("Enter your taxable income: ");
                long income = input.nextLong();
                TaxAmount tax = new TaxAmount(income, status);
                tax.compute();
                System.out.println("Amount of tax: " + tax.getTaxAmount());
                retry = false; //exit loop
                input.close(); //close resources
                break;
            default:
                System.out.println("Error: Incorrect filing status! Please try Again.");
                break;
            }
        }
        
    }
    
}
