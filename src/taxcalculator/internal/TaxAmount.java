/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxcalculator.internal;

/**
 *
 * @author Archimage
 */
public class TaxAmount {
    
    //Look up table for the upper bound of each bracket
    public static final long[][] TAX_BRACKET = 
        {{9075, 36900, 89350, 186350, 405100, 406750},
        {18150, 73800, 148850, 226850, 405100, 457600},
        {9075, 36900, 74425, 113425, 202550, 228800}, 
        {12950, 49400, 127550, 206600, 405100, 432200}};
    //Look up table for the tax percentage of each bracket
    public static final double[] TAX_PERCENTAGE = 
        {0.1, 0.15, 0.25, 0.28, 0.33, 0.35, 0.396};
    //Fields
    private double taxedAmount;
    private final long income;
    private final int index;
    
    public TaxAmount(long income, int index) {
        this.income = income;
        this.index = index;
    }
    
    public void compute(){
        //Loop going through every case except the largest tax bracket
        for(int i = 0; i < TAX_BRACKET[index].length; i++){
            long taxable = income - TAX_BRACKET[index][i];
            //When taxable is negative, income is below the tax bracket.
            //When income is lower than the first bracket, tax percentage of the
            //first bracket is multiplied with the income.
            if(taxable < 0 && i == 0){
                taxedAmount = calculate(TAX_PERCENTAGE[i], income, 0);
                break; //exit the loop
            }
            //When income is lower than any except the first bracket, 
            //tax percentage of the bracket is multiplied with the difference
            //between the income and the previous bracket
            else if (taxable < 0){
                taxedAmount += calculate(TAX_PERCENTAGE[i], 
                        income, TAX_BRACKET[index][i-1]);
                break; //exit the loop
            }
            //When taxable is higher than the first bracket, tax percentage of 
            //the first bracket is multiplied with the upper bound of the first
            //bracket.
            else if (taxable >= 0 && i == 0){
                taxedAmount += calculate(TAX_PERCENTAGE[i], 
                        TAX_BRACKET[index][i], 0);
            }
            //When income is larger than the current bracket, the tax percentage
            //of the bracket is multiplied with the upper bound of the bracket.
            else{
                taxedAmount += calculate(TAX_PERCENTAGE[i], 
                        TAX_BRACKET[index][i], TAX_BRACKET[index][i-1]);
            }    
        }
        //Special case for income larger than the largest tax bracket.
        //Variable bracketLength stores the number of column in the look up 
        //table for greater readability.
        int bracketLength = TAX_BRACKET[index].length;
        if(income > TAX_BRACKET[index][bracketLength - 1]){
            taxedAmount += calculate(TAX_PERCENTAGE[bracketLength], 
                        income, TAX_BRACKET[index][bracketLength - 1]);
        }
    }
    
    private double calculate(double percentage, long taxed, long lowerBound) {
        return percentage * (taxed - lowerBound);
    }
    
    public double getTaxAmount(){
        return taxedAmount;
    }
}
