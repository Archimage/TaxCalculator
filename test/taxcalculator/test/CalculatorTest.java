/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxcalculator.test;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import taxcalculator.internal.TaxAmount;

/**
 *
 * @author Archimage
 */
public class CalculatorTest {
    
    public CalculatorTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    //This is to test option zero
    public void testZero(){
        TaxAmount taxLow = new TaxAmount(3000, 0);
        taxLow.compute();
        assertEquals(300, taxLow.getTaxAmount(), 0.01);
        TaxAmount taxMid = new TaxAmount(150000, 0);
        taxMid.compute();
        assertEquals(35175.75, taxMid.getTaxAmount(), 0.01);
        TaxAmount taxHigh = new TaxAmount(450000, 0);
        taxHigh.compute();
        assertEquals(135245.75, taxHigh.getTaxAmount(), 0.01);
    }
    
    @Test
    //This is to test option one
    public void testOne(){
        TaxAmount taxLow = new TaxAmount(3000, 1);
        taxLow.compute();
        assertEquals(300, taxLow.getTaxAmount(), 0.01);
        TaxAmount taxMid = new TaxAmount(150000, 1);
        taxMid.compute();
        assertEquals(29247, taxMid.getTaxAmount(), 0.01);
        TaxAmount taxHigh = new TaxAmount(450000, 1);
        taxHigh.compute();
        assertEquals(125302.5, taxHigh.getTaxAmount(), 0.01);
    }
    
    @Test
    //This is to test option two
    public void testTwo(){
        TaxAmount taxLow = new TaxAmount(3000, 2);
        taxLow.compute();
        assertEquals(300, taxLow.getTaxAmount(), 0.01);
        TaxAmount taxMid = new TaxAmount(150000, 2);
        taxMid.compute();
        assertEquals(37452.25, taxMid.getTaxAmount(), 0.01);
        TaxAmount taxHigh = new TaxAmount(450000, 2);
        taxHigh.compute();
        assertEquals(151576.45, taxHigh.getTaxAmount(), 0.01);
    }
    
    @Test
    //This is to test option three
    public void testThree(){
        TaxAmount taxLow = new TaxAmount(3000, 3);
        taxLow.compute();
        assertEquals(300, taxLow.getTaxAmount(), 0.01);
        TaxAmount taxMid = new TaxAmount(150000, 3);
        taxMid.compute();
        assertEquals(32586, taxMid.getTaxAmount(), 0.01);
        TaxAmount taxHigh = new TaxAmount(450000, 3);
        taxHigh.compute();
        assertEquals(130472.8, taxHigh.getTaxAmount(), 0.01);
    }
}
